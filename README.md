### Install

As requirements you need `nodejs` and `npm`. 

    npm install -g apaste

In Debian GNU/Linux 8 (Jessie) the npm version is too old to work with. You will need to upgrade npm manually.

    # install requirements
    apt install nodejs nodejs-legacy npm
    npm install -g npm            # install npm 3.3.x
    hash -r                       # rescan $PATH
    # finally install apaste
    npm install -g apaste

### apaste, the share.riseup.net command-line client

This is a modification of [upclient](https://github.com/Upload/upclient), the [Up1](https://github.com/Upload/Up1) command-line client to make it work with [https://share.riseup.net](https://share.riseup.net). 

Please help and contribute directly to the [Up1 command-line client](https://github.com/Upload/upclient)

    Usage: apaste [options] [files]
    
    Upload files and text to an Up1 based pastebin. If no argument is specified, stdin is assumed.
    
    Options:
      -b, --binary            force application/octet-stream (for downloadable file)
      -t, --text              force text/plain (for pastebin)
      -f, --file <name>       force file name for stdin based inputs
      -m, --mime <mime>       force given mime type (default: detect)
      -T, --tor               route the upload through Tor.
      -h, --hidden-service    use hidden service address
          --version       display version information and exit
          --help          display this help and exit

### Configuration

You can customize some options in the at `~/.apasterc`. This is a list of possible options (with their default values)

```
server_address = https://share.riseup.net
hidden_service_address = http://6zc6sejeho3fwrd4.onion
api_key = 59Mnk5nY6eCn4bi9GvfOXhMH54E7Bh6EMJXtyJfs
tor = false
hidden_service = false
```

The `tor` and `hidden_service` variables means always route the upload through Tor and always use the hidden service address respectively. If `hidden_service = true` it tor will be always true too.

### Usage examples

Paste command output to Up1:

    ps aux | apaste

Copy an image file to Up1:

    apaste image.png

Take a screenshot (using a selection rectangle), send it to Up1, and put the result link on the clipboard:

    import png:- | apaste | xsel -b

Do the same as above, but also notify when complete:

    import png:- | apaste | tee >(xsel -b) >(xargs notify-send "Upload Complete")

Use the Tor network

    echo "Hi Tor" | apaste -T

Use the Hidden service address

    echo "Hi Tor" | apaste -h

(when you use the hidden service you will get the url with the hidden service address too)

### Up1

For more information on Up1, view the README at https://github.com/Upload/Up1
